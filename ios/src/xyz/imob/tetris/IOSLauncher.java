package xyz.imob.tetris;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;

import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.foundation.NSError;
import org.robovm.apple.foundation.NSErrorException;
import org.robovm.apple.foundation.NSObject;
import org.robovm.apple.gamekit.GKAchievement;
import org.robovm.apple.gamekit.GKLeaderboard;
import org.robovm.apple.uikit.UIApplication;
import org.robovm.apple.uikit.UIApplicationLaunchOptions;
import org.robovm.pods.flurry.analytics.Flurry;
import org.robovm.pods.flurry.analytics.FlurryLogLevel;
import org.robovm.pods.google.GGLContext;
import org.robovm.pods.google.mobileads.GADInterstitial;
import org.robovm.pods.google.mobileads.GADInterstitialDelegateAdapter;
import org.robovm.pods.google.mobileads.GADRequest;
import org.robovm.pods.google.mobileads.GADRequestError;

import java.util.ArrayList;

public class IOSLauncher extends IOSApplication.Delegate {


    private static final String GOOGLE_CP_ID = "ca-app-pub-1209910642835439/9236487704";
    private static final String RANK_SCORE_ID = "tetris_rank";

    private static final String FLURRY_API_KEY = "N55J8CM7T6FHQVPXWQQB";

    private GADInterstitial gadInterstitial;
    private GameCenterManager gcManager;


    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();

        gadInterstitial = createAndLoadInterstitial();
        return new IOSApplication(new Tetris(new Tetris.ADHandler() {
            @Override
            public void showAD() {
                if (gadInterstitial == null) {
                    gadInterstitial = createAndLoadInterstitial();
                    return;
                }
                if (gadInterstitial.isReady()) {
                    gadInterstitial.present(UIApplication.getSharedApplication().getKeyWindow().getRootViewController());
                }
            }
        }, new Tetris.ReportScoreHandler() {
            @Override
            public void reportScore(int score) {
                gcManager.reportScore(RANK_SCORE_ID, score);
            }

            @Override
            public void showLeaderboard() {
                gcManager.showLeaderboardsView();
            }

        }), config);
    }

    private GADInterstitial createAndLoadInterstitial() {

        gadInterstitial = new GADInterstitial(GOOGLE_CP_ID);
        gadInterstitial.setDelegate(new GADInterstitialDelegateAdapter() {
            @Override
            public void didReceiveAd(GADInterstitial ad) {

            }

            @Override
            public void didFailToReceiveAd(GADInterstitial ad, GADRequestError error) {

            }

            @Override
            public void willPresentScreen(GADInterstitial ad) {

            }

            @Override
            public void didFailToPresentScreen(GADInterstitial ad) {

            }

            @Override
            public void willDismissScreen(GADInterstitial ad) {

            }

            @Override
            public void didDismissScreen(GADInterstitial ad) {
                gadInterstitial = createAndLoadInterstitial();
            }

            @Override
            public void willLeaveApplication(GADInterstitial ad) {

            }

            @Override
            public boolean shouldChangeAudioSessionToCategory(NSObject ad, String audioSessionCategory) {
                return false;
            }
        });

        GADRequest request = new GADRequest();
        gadInterstitial.loadRequest(request);
        return gadInterstitial;
    }

    @Override
    public boolean didFinishLaunching(UIApplication application, UIApplicationLaunchOptions launchOptions) {
        initGoogleService();
        initAndLoginGameCenter();
        initFlurry();
        return super.didFinishLaunching(application, launchOptions);
    }

    private void initFlurry() {
        Flurry.setDebugLogEnabled(false);
        Flurry.setLogLevel(FlurryLogLevel.None);
        Flurry.startSession(FLURRY_API_KEY);
    }

    private void initAndLoginGameCenter() {
        gcManager = new GameCenterManager(new GameCenterListener() {
            @Override
            public void playerLoginFailed(NSError error) {
                System.out.println("playerLoginFailed. error: " + error);
            }

            @Override
            public void playerLoginCompleted() {
                System.out.println("playerLoginCompleted");
            }

            @Override
            public void achievementReportCompleted() {
                System.out.println("achievementReportCompleted");
            }

            @Override
            public void achievementReportFailed(NSError error) {
                System.out.println("achievementReportFailed. error: " + error);
            }

            @Override
            public void achievementsLoadCompleted(ArrayList<GKAchievement> achievements) {
                System.out.println("achievementsLoadCompleted: " + achievements.size());
            }

            @Override
            public void achievementsLoadFailed(NSError error) {
                System.out.println("achievementsLoadFailed. error: " + error);
            }

            @Override
            public void achievementsResetCompleted() {
                System.out.println("achievementsResetCompleted");
            }

            @Override
            public void achievementsResetFailed(NSError error) {
                System.out.println("achievementsResetFailed. error: " + error);
            }

            @Override
            public void scoreReportCompleted() {
                System.out.println("scoreReportCompleted");
            }

            @Override
            public void scoreReportFailed(NSError error) {
                System.out.println("scoreReportFailed. error: " + error);
            }

            @Override
            public void leaderboardsLoadCompleted(ArrayList<GKLeaderboard> scores) {
                System.out.println("scoresLoadCompleted: " + scores.size());
            }

            @Override
            public void leaderboardsLoadFailed(NSError error) {
                System.out.println("scoresLoadFailed. error: " + error);
            }

            @Override
            public void leaderboardViewDismissed() {
                System.out.println("leaderboardViewDismissed");
            }

            @Override
            public void achievementViewDismissed() {
                System.out.println("achievementViewDismissed");
            }
        });
        gcManager.login();
    }

    private void initGoogleService() {
        try {
            GGLContext.getSharedInstance().configure();
        } catch (NSErrorException e) {
            System.err.println("Error configuring the Google context: " + e.getError());
        }
    }


    public static void main(String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.close();
    }
}