package xyz.imob.tetris;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import xyz.imob.tetris.others.Constants;
import xyz.imob.tetris.utils.ResourceLoader;

public class Tetris extends Game {

    public static Tetris tetris;

    private SpriteBatch spriteBatch;
    private OrthographicCamera camera;
    private Viewport viewport;

    private ADHandler adHandler;
    private ReportScoreHandler reportScoreHandler;


    public Tetris(ADHandler adHandler, ReportScoreHandler reportScoreHandler) {
        this.adHandler = adHandler;
        this.reportScoreHandler = reportScoreHandler;
    }


    public void showAD() {
        if (adHandler != null) {
            adHandler.showAD();
        }
    }

    public void reportScore(int score) {
        if (reportScoreHandler != null) {
            reportScoreHandler.reportScore(score);
        }
    }

    public void showLeaderboard() {
        if (reportScoreHandler != null) {
            reportScoreHandler.showLeaderboard();
        }
    }
    

    @Override
    public void create() {
        ResourceLoader.loadResource();
        tetris = this;

        spriteBatch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.position.set(Constants.WIDTH / 2, Constants.HEIGHT / 2, 0);
        viewport = new StretchViewport(Constants.WIDTH, Constants.HEIGHT, camera);

        setScreen(new GameScreen());
    }


    public SpriteBatch getSpriteBatch() {
        return spriteBatch;
    }

    public Viewport getViewport() {
        return viewport;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(.529411765f, .592156863f, .450980392f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        super.render();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height);
    }

    @Override
    public void dispose() {
        tetris = null;
        spriteBatch.dispose();
        ResourceLoader.freeResource();
    }

    public interface ADHandler {
        void showAD();
    }

    public interface ReportScoreHandler {
        void reportScore(int score);

        void showLeaderboard();
    }

}
