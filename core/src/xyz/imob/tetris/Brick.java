package xyz.imob.tetris;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import xyz.imob.tetris.utils.ActorHelper;
import xyz.imob.tetris.utils.ResourceLoader;
import xyz.imob.tetris.utils.SizeUtils;

/**
 * Created by glzlaohuai on 2017/2/19.
 */

public class Brick extends Image {

    private static final Vector2 BLOCK_SIZE = SizeUtils.formatSize(39, 39, false);
    private static final Vector2 BLOCK_MARGIN = SizeUtils.formatSize(1, 1, false);


    private int locX;
    private int locY;

    public Brick(int x, int y) {
        super(ResourceLoader.blockFill);
        ActorHelper.setSize(this, 39, 39, false);
        this.locX = x;
        this.locY = y;

        setPosition(GameScreen.BLOCK_START_POSITION.x + y * (BLOCK_SIZE.x + BLOCK_MARGIN.x), GameScreen.BLOCK_START_POSITION.y + x * (BLOCK_SIZE.y + BLOCK_MARGIN.y));
        setVisible(false);
    }

    public void setLocX(int locX) {
        this.locX = locX;
        setY(GameScreen.BLOCK_START_POSITION.y + locX * (BLOCK_SIZE.y + BLOCK_MARGIN.y));
    }

    public void setLocY(int locY) {
        this.locY = locY;
        setX(GameScreen.BLOCK_START_POSITION.x + locY * (BLOCK_SIZE.x + BLOCK_MARGIN.x));
    }


    public int getLocX() {
        return locX;
    }

    public int getLocY() {
        return locY;
    }

    public void moveLeft() {
        if (locY > 0) {
            locY--;
            setX(getX() - BLOCK_SIZE.x - BLOCK_MARGIN.x);
        }
    }

    public void moveRight() {
        if (locY < 9) {
            locY++;
            setX(getX() + BLOCK_SIZE.x + BLOCK_MARGIN.x);
        }
    }

    public void moveDown() {

        if (locX > 0) {
            locX--;
            setY(getY() - BLOCK_SIZE.y - BLOCK_MARGIN.y);
        }

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        checkVisible();
    }

    private void checkVisible() {
        if (locX >= 19) {
            setVisible(false);
        } else {
            setVisible(true);
        }
    }

    @Override
    public String toString() {
        return "x:" + locX + ",y:" + locY;
    }
}
