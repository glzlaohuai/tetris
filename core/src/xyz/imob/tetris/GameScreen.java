package xyz.imob.tetris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

import net.mwplay.nativefont.NativeLabel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import xyz.imob.tetris.others.Constants;
import xyz.imob.tetris.shapes.AbstractShape;
import xyz.imob.tetris.utils.ActorHelper;
import xyz.imob.tetris.utils.GameSaver;
import xyz.imob.tetris.utils.I18NUtils;
import xyz.imob.tetris.utils.ResourceLoader;
import xyz.imob.tetris.utils.ShapeFactory;
import xyz.imob.tetris.utils.SizeUtils;
import xyz.imob.tetris.utils.SoundUtils;

/**
 * Created by glzlaohuai on 2017/2/18.
 */

public class GameScreen implements Screen {

    private static final String TAG = "GameScreen";

    private static final Vector2 BLOCK_SIZE = SizeUtils.formatSize(39, 39, false);
    public static Vector2 BLOCK_START_POSITION = new Vector2();

    //每300ms移动一格
    public static final int INIT_SPEED = 550;
    //每次升级，移动间隔时间缩短20ms
    public static final int SPEED_STEP = 50;
    private static final int EACH_CLEAR_SCORE = 100;

    private int rankClickNum = 0;

    private Stage stage;
    private Tetris tetris;

    private Image soundImg;
    private Image pauseImg;

    private Button rotateBtn;

    private Button left;
    private Button top;
    private Button right;
    private Button bottom;

    private Button startPauseBtn;
    private Button soundBtn;
    private Button rankBtn;
    private Button moreBtn;

    private Map<Integer, Image> effectMap = new HashMap<Integer, Image>();

    private Label hiScoreLabel;
    private Label nowScoreLabel;
    private Label levelLabel;

    private int level = 1;
    //初始速度
    private int score;
    private boolean isGameOver = true;
    private boolean isPause = false;

    private TimeTicker timeTicker = new TimeTicker(INIT_SPEED);

    //保存着所有的方块
    private Array<Brick> bricksArray = new Array<Brick>();
    private Map<Integer, Array<Brick>> bricksMap = new HashMap<Integer, Array<Brick>>();

    private xyz.imob.tetris.shapes.AbstractShape shape;
    private xyz.imob.tetris.shapes.AbstractShape nextShape;

    private boolean isLeftTouch = false;
    private boolean isRightTouch = false;
    private boolean isDownTouch = false;
    private boolean isDownTouchTimeCaculate = true;

    private long leftDownTime;
    private long rightDownTime;
    private long downDownTime;

    private Group nextShapeGroup = new Group();
    private Group brickGroup = new Group();


    public GameScreen() {
        this.tetris = Tetris.tetris;

        buildUI();

        startOrPauseGame();
    }


    class XStage extends Stage {

        public XStage(Viewport viewport, Batch batch) {
            super(viewport, batch);
        }

        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {

            Vector3 touchPos = new Vector3(screenX, screenY, 0);
            getViewport().getCamera().unproject(touchPos);

            Image touchEffect = effectMap.get(pointer);

            if (touchEffect != null) {
                touchEffect.remove();
                effectMap.put(pointer, null);
            }

            if (touchPos.x >= 214 && touchPos.x <= 875 && touchPos
                    .y >= 1000 && touchPos.y <= 1747) {
                touchEffect = new Image(ResourceLoader.touchEffect);
                ActorHelper.setSize(touchEffect, 400, 419);
                touchEffect.setPosition(touchPos.x, touchPos.y, Align.center);
                addActor(touchEffect);
                effectMap.put(pointer, touchEffect);
            }

            return super.touchDown(screenX, screenY, pointer, button);
        }

        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {

            Image touchEffect = effectMap.get(pointer);

            if (touchEffect != null) {
                touchEffect.remove();
                effectMap.put(pointer, null);
            }

            return super.touchUp(screenX, screenY, pointer, button);
        }

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            Vector3 touchPos = new Vector3(screenX, screenY, 0);
            getViewport().getCamera().unproject(touchPos);

            Image touchEffect = effectMap.get(pointer);

            if (touchPos.x >= 214 && touchPos.x <= 875 && touchPos
                    .y >= 1000 && touchPos.y <= 1747) {
                if (touchEffect != null) {
                    touchEffect.setPosition(touchPos.x, touchPos.y, Align.center);
                } else {
                    touchEffect = new Image(ResourceLoader.touchEffect);
                    ActorHelper.setSize(touchEffect, 400, 419);
                    touchEffect.setPosition(touchPos.x, touchPos.y, Align.center);
                    addActor(touchEffect);
                    effectMap.put(pointer, touchEffect);
                }
            } else {
                if (touchEffect != null) {
                    touchEffect.remove();
                    effectMap.put(pointer, null);
                }
            }
            return super.touchDragged(screenX, screenY, pointer);
        }
    }


    private void buildUI() {

        stage = new XStage(tetris.getViewport(), tetris.getSpriteBatch());

        Gdx.input.setInputProcessor(stage);

        Image bg = new Image(ResourceLoader.bg);
        bg.setSize(Constants.WIDTH, Constants.HEIGHT);
        bg.setPosition(0, 0);
        stage.addActor(bg);

        //黑线框
        Image blackLineFrame = new Image(ResourceLoader.outLine);
        System.out.println(Gdx.graphics.getWidth() + "," + Gdx.graphics.getHeight());
        System.out.println(blackLineFrame.getWidth() + "," + blackLineFrame.getHeight());
        blackLineFrame.setPosition(152, 923);
        stage.addActor(blackLineFrame);

        //block区域
        Image blockAreaFrame = new Image(ResourceLoader.frame);
        ActorHelper.setSize(blockAreaFrame, 405, 768, false);
        blockAreaFrame.setPosition(214, 986);
        stage.addActor(blockAreaFrame);

        //next背景
        Image nextShapeBg = new Image(ResourceLoader.nextShapeBg);
        ActorHelper.setSize(nextShapeBg, 161, 159, false);
        nextShapeBg.setPosition(687, 1170);
        stage.addActor(nextShapeBg);

        stage.addActor(nextShapeGroup);

        Image blockBg = new Image(ResourceLoader.blockBg);
        ActorHelper.setSize(blockBg, 399, 759, false);


        Vector2 blockXMargin = SizeUtils.formatSize(3, 3, false);
        BLOCK_START_POSITION.x = 214 + blockXMargin.x;
        BLOCK_START_POSITION.y = 986 + 5;

        blockBg.setPosition(BLOCK_START_POSITION.x, BLOCK_START_POSITION.y);
        stage.addActor(blockBg);

        soundImg = new Image(ResourceLoader.sound);
        ActorHelper.setSize(soundImg, 43, 51);
        soundImg.setPosition(692, 1016);
        stage.addActor(soundImg);

        pauseImg = new Image(ResourceLoader.coffee);
        ActorHelper.setSize(pauseImg, 62, 54);
        pauseImg.setPosition(768, 1016);
        stage.addActor(pauseImg);
        pauseImg.setColor(pauseImg.getColor().r, pauseImg.getColor().g, pauseImg.getColor().b, 0.5f);

        //最高分
        Label.LabelStyle labelStyle = new Label.LabelStyle(ResourceLoader.font40, Color.BLACK);
        hiScoreLabel = new Label("" + GameSaver.getHighestScore(), labelStyle);
        hiScoreLabel.setPosition(nextShapeBg.getX(Align.center), nextShapeBg.getY(Align.top) + 20, Align.center | Align.bottom);
        hiScoreLabel.setAlignment(Align.center | Align.bottom);
        stage.addActor(hiScoreLabel);

        Label highScoreTitle = new Label("HI-SCORE", labelStyle);
        highScoreTitle.setPosition(nextShapeBg.getX(Align.center), hiScoreLabel.getY(Align.top) + 1, Align.center | Align.bottom);
        stage.addActor(highScoreTitle);

        nowScoreLabel = new Label("" + score, labelStyle);
        nowScoreLabel.setPosition(nextShapeBg.getX(Align.center), highScoreTitle.getY(Align.top) + 1, Align.center | Align.bottom);
        nowScoreLabel.setAlignment(Align.center);
        stage.addActor(nowScoreLabel);

        Label nowScoreLabelTitle = new Label("SCORE", labelStyle);
        nowScoreLabelTitle.setPosition(nextShapeBg.getX(Align.center), nowScoreLabel.getY(Align.top) + 1, Align.center | Align.bottom);
        stage.addActor(nowScoreLabelTitle);

        levelLabel = new Label("" + level, labelStyle);
        levelLabel.setPosition(nextShapeBg.getX(Align.center), nowScoreLabelTitle.getY(Align.top) + 1, Align.center | Align.bottom);
        levelLabel.setAlignment(Align.center | Align.bottom);
        stage.addActor(levelLabel);

        Label levelTitle = new Label("LEVEL", labelStyle);
        levelTitle.setPosition(nextShapeBg.getX(Align.center), levelLabel.getY(Align.top) + 1, Align.center | Align.bottom);
        stage.addActor(levelTitle);


        left = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowPress)), null);
        ActorHelper.setSize(left, 238, 262);
        left.setPosition(31, 246);
        stage.addActor(left);

        top = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowPress)), null);
        ActorHelper.setSize(top, 238, 262);
        top.setPosition(192, 420);
        stage.addActor(top);

        right = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowPress)), null);
        ActorHelper.setSize(right, 238, 262);
        right.setPosition(354, 246);
        stage.addActor(right);

        bottom = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowPress)), null);
        ActorHelper.setSize(bottom, 238, 262);
        bottom.setPosition(192, 72);
        stage.addActor(bottom);

        rotateBtn = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnYellowPress)), null);
        ActorHelper.setSize(rotateBtn, 354, 390);
        rotateBtn.setPosition(677, 200);
        stage.addActor(rotateBtn);

        left.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (isGameOver || isPause) return;
                SoundUtils.playSound(ResourceLoader.moveSound);
                if (shape != null) shape.moveLeft();
                leftDownTime = System.currentTimeMillis();
                super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                isLeftTouch = false;
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                isLeftTouch = true;
                return super.longPress(actor, x, y);
            }
        });


        right.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (isGameOver || isPause) return;
                SoundUtils.playSound(ResourceLoader.moveSound);
                if (shape != null) shape.moveRight();
                rightDownTime = System.currentTimeMillis();
                super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                isRightTouch = false;
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                isRightTouch = true;
                return super.longPress(actor, x, y);
            }
        });

        rotateBtn.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (isGameOver || isPause) return;
                SoundUtils.playSound(ResourceLoader.rotateSound);
                if (shape != null) shape.rotate();
                super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
            }
        });

        bottom.addListener(new ActorGestureListener() {
            @Override
            public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (isGameOver || isPause) return;
                SoundUtils.playSound(ResourceLoader.moveSound);
                moveDownByShapeState(shape);
                downDownTime = System.currentTimeMillis();

                super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                isDownTouch = false;
                isDownTouchTimeCaculate = true;
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public boolean longPress(Actor actor, float x, float y) {
                isDownTouch = true;
                return super.longPress(actor, x, y);
            }
        });


        top.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if (isGameOver || isPause) return;
                if (shape != null) directMoveDown();
            }
        });


        startPauseBtn = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnRedNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnRedPress)), null);
        ActorHelper.setSize(startPauseBtn, 83, 83);
        startPauseBtn.setPosition(564, 619);
        stage.addActor(startPauseBtn);

        NativeLabel startPauseLabel = new NativeLabel(I18NUtils.getValue("start_pause"), ResourceLoader.font25, Color.BLACK);
        startPauseLabel.setPosition(startPauseBtn.getX(Align.center), startPauseBtn.getY(Align.bottom) - 10, Align.center | Align.top);
        stage.addActor(startPauseLabel);

        soundBtn = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnGreenNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnGreenPress)), null);
        ActorHelper.setSize(soundBtn, 83, 83);
        soundBtn.setPosition(685, 619);
        setSoundAlpha();
        stage.addActor(soundBtn);

        NativeLabel soundLabel = new NativeLabel(I18NUtils.getValue("sound"), ResourceLoader.font25, Color.BLACK);
        soundLabel.setPosition(soundBtn.getX(Align.center), soundBtn.getY(Align.bottom) - 10, Align.center | Align.top);
        stage.addActor(soundLabel);


        rankBtn = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnGreenNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnGreenPress)), null);
        ActorHelper.setSize(rankBtn, 83, 83);
        rankBtn.setPosition(121 + 685, 619);
        stage.addActor(rankBtn);

        NativeLabel rankLabel = new NativeLabel(I18NUtils.getValue("rank"), ResourceLoader.font25, Color.BLACK);
        rankLabel.setPosition(rankBtn.getX(Align.center), rankBtn.getY(Align.bottom) - 10, Align.center | Align.top);
        stage.addActor(rankLabel);

        moreBtn = new Button(new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnGreenNormal)), new TextureRegionDrawable(new TextureRegion(ResourceLoader.btnGreenPress)), null);
        ActorHelper.setSize(moreBtn, 83, 83);
        moreBtn.setPosition(121 * 2 + 685, 619);
        stage.addActor(moreBtn);

        NativeLabel moreLabel = new NativeLabel(I18NUtils.getValue("more"), ResourceLoader.font25, Color.BLACK);
        moreLabel.setPosition(moreBtn.getX(Align.center), moreBtn.getY(Align.bottom) - 10, Align.center | Align.top);
        stage.addActor(moreLabel);

        stage.addActor(brickGroup);


        startPauseBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                startOrPauseGame();

            }
        });


        soundBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                toggleSound();
            }
        });

        rankBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                rankClickNum++;
                Tetris.tetris.showLeaderboard();
                if (++rankClickNum % 3 == 0) {
                    showAD();
                }
            }
        });
        moreBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                switch (Gdx.app.getType()) {
                    case Android:
                        Gdx.app.getNet().openURI(Constants.ANDROID_MORE);
                        break;
                    case iOS:
                        Gdx.app.getNet().openURI(Constants.IOS_MORE);
                        break;
                }
            }
        });

    }

    private void directMoveDown() {
        if (shape != null) {
            AbstractShape.HitState hitState = shape.checkHitState();
            switch (hitState) {
                case HIT_NONE:
                    shape.moveDown();
                    directMoveDown();
                    break;
                case WILL_HIT:
                case HIT:
                    shapeHit();
                    brickGroup.addAction(Actions.sequence(Actions.moveBy(0, 10, 0.05f, Interpolation.swingOut), Actions.moveBy(0, -10, 0.05f, Interpolation.swingOut)));
                    break;
            }
        }
    }

    private void toggleSound() {
        GameSaver.setSound(!GameSaver.isSoundOpen());
        SoundUtils.refreshSoundOnValue();
        SoundUtils.stopMusic(ResourceLoader.bgMusic);
        setSoundAlpha();
    }

    private void setSoundAlpha() {
        soundImg.setColor(soundBtn.getColor().r, soundBtn.getColor().g, soundBtn.getColor().b, GameSaver.isSoundOpen() ? 1 : 0.5f);
    }

    private void resetGame() {
        isPause = false;
        score = 0;
        level = 1;
        timeTicker.setTickMs(INIT_SPEED);
        levelLabel.setText("" + level);
        nowScoreLabel.setText("" + score);
        shape = null;
    }

    private void startOrPauseGame() {
        if (isGameOver) {
            brickGroup.clear();
            bricksMap.clear();
            bricksArray.clear();
            resetGame();
            isGameOver = false;
            SoundUtils.playMusic(ResourceLoader.bgMusic);

        } else {
            isPause = !isPause;
            if (isPause) {
                //展示广告
                showAD();
            }
            pauseImg.setColor(pauseImg.getColor().r, pauseImg.getColor().g, pauseImg.getColor().b, isPause ? 1f : 0.5f);
        }
    }

    private void showAD() {
        Tetris.tetris.showAD();
    }


    private void moveDownByShapeState(AbstractShape shape) {
        if (shape != null) {
            AbstractShape.HitState hitState = shape.checkHitState();

            switch (hitState) {
                case HIT_NONE:
                    if (isDownTouch) {
                        SoundUtils.playSound(ResourceLoader.moveSound);
                    }
                    shape.moveDown();
                    break;
                case WILL_HIT:
                    if (isDownTouch) {
                        shapeHit();
                    }
                    break;
                case HIT:
                    shapeHit();
                    break;
            }
        }
    }

    private void checkLongPressState() {
        long time = System.currentTimeMillis();
        long pressTime = 0;
        if (left.isPressed()) {
            pressTime = time - leftDownTime;
            if (pressTime > 300) {
                isLeftTouch = true;
            }
        }
        if (right.isPressed()) {
            pressTime = time - rightDownTime;
            if (pressTime > 300) {
                isRightTouch = true;
            }
        }
        if (bottom.isPressed() && isDownTouchTimeCaculate) {
            pressTime = time - downDownTime;
            if (pressTime > 300) {
                isDownTouch = true;
            }
        }
    }


    private void update(float delta) {
        stage.act(delta);
        if (!isGameOver && !isPause) {
            checkLongPressState();
            checkShapeAndNextShapeExistState();
            doActionByUserAct();
            timeTicker.tick(delta);
            if (timeTicker.trigger() && !isDownTouch) {
                moveDownByShapeState(shape);
            }
        }
    }

    private void doActionByUserAct() {
        if (shape == null) return;
        if (isLeftTouch && shape.canMoveLeft()) {
            shape.moveLeft();
            SoundUtils.playSound(ResourceLoader.moveSound);
        }
        if (isRightTouch && shape.canMoveRight()) {
            shape.moveRight();
            SoundUtils.playSound(ResourceLoader.moveSound);
        }
        if (isDownTouch) {
            moveDownByShapeState(shape);
        }
    }


    private void shapeHit() {
        SoundUtils.playSound(ResourceLoader.hitSound);
        absorbShape(shape);
        checkClear();
        checkGameState();
        if (!isGameOver) {
            showNextShapeAndGenerateAnotherNext();
        }
    }

    private void checkClear() {
        Set<Map.Entry<Integer, Array<Brick>>> entrySet = bricksMap.entrySet();

        List<Integer> fullRowArray = new ArrayList<Integer>();

        for (Map.Entry<Integer, Array<Brick>> entry : entrySet) {
            //满了，记录
            if (entry.getValue() != null && entry.getValue().size == 10) {
                fullRowArray.add(entry.getKey());
            }
        }
        updateScore(fullRowArray.size());
        for (Integer row : fullRowArray) {
            clearBricks(bricksMap.get(row));
            bricksArray.removeAll(bricksMap.get(row), true);
            bricksMap.put(row, null);
        }
        rearrangeBricks(fullRowArray);
    }

    private void rearrangeBricks(List<Integer> fullRowArray) {
        Collections.sort(fullRowArray, Collections.<Integer>reverseOrder());
        for (Integer row : fullRowArray) {
            for (int i = row + 1; i <= 18; i++) {
                Array<Brick> topBricks = bricksMap.get(i);
                if (topBricks != null && topBricks.size != 0) {
                    for (Brick brick : topBricks) {
                        brick.moveDown();
                    }
                }
                bricksMap.put(i - 1, bricksMap.get(i));
            }
        }
    }

    private void clearBricks(Array<Brick> array) {
        for (Brick brick : array) {
            brick.remove();
        }
    }

    private void updateScore(int line) {
        score += line * EACH_CLEAR_SCORE;
        nowScoreLabel.setText(score + "");

        int nowLevel = Math.min(score / 3000 + 1, 10);
        if (nowLevel != level) {
            level = nowLevel;
            levelLabel.setText(level + "");
            timeTicker.setTickMs(INIT_SPEED - SPEED_STEP * (level - 1));
            levelShine();
            if (MathUtils.randomBoolean()) {
                showAD();
            }
            SoundUtils.playSound(ResourceLoader.clearSound);
        }
    }

    private void levelShine() {
        levelLabel.addAction(Actions.repeat(3, Actions.sequence(Actions.hide(), Actions.delay(0.5f), Actions.show(), Actions.delay(0.5f))));
    }


    private void checkGameState() {
        if (bricksMap.get(19) != null && bricksMap.get(19).size != 0) {
            isGameOver = true;
            SoundUtils.playSound(ResourceLoader.gameOverSound);
            SoundUtils.stopMusic(ResourceLoader.bgMusic);
            postScore();
            saveHighScore();
            resetGame();
            playGameOverActions();
            showAD();
        }
    }

    private void postScore() {
        Tetris.tetris.reportScore(score);
    }

    private void saveHighScore() {
        if (score > GameSaver.getHighestScore()) {
            GameSaver.saveHighestScore(score);
            hiScoreLabel.setText(score + "");
            highScoreLabelShine();
        }
    }

    private void highScoreLabelShine() {
        hiScoreLabel.addAction(Actions.repeat(3, Actions.sequence(Actions.hide(), Actions.delay(0.5f), Actions.show(), Actions.delay(0.5f))));
    }


    boolean rowContianBrick(Array<Brick> brickArray, int y) {
        if (brickArray == null || brickArray.size == 0) {
            return false;
        } else {
            if (brickArray.size == 10) return true;
            for (Brick brick :
                    brickArray) {
                if (brick.getLocY() == y) {
                    return true;
                }
            }
        }
        return false;
    }


    private void playGameOverActions() {

        Action actions[] = new Action[38];
        //上去
        for (int i = 0; i < 38; i++) {
            final int finalI = i;

            actions[i] = Actions.run(new Runnable() {
                @Override
                public void run() {
                    //该行的方块列表
                    if (finalI < 19) {
                        Array<Brick> bricks = bricksMap.get(finalI);
                        if (bricks == null) {
                            bricks = new Array<Brick>();
                            for (int j = 0; j < 10; j++) {
                                Brick brick = new Brick(finalI, j);
                                bricksArray.add(brick);
                                brickGroup.addActor(brick);
                                bricks.add(brick);
                            }
                        } else {
                            for (int j = 0; j < 10; j++) {
                                if (!rowContianBrick(bricks, j)) {
                                    Brick brick = new Brick(finalI, j);
                                    bricksArray.add(brick);
                                    bricks.add(brick);
                                    brickGroup.addActor(brick);
                                }
                            }
                        }
                        bricksMap.put(finalI, bricks);
                    } else {
                        int removeX = 18 - (finalI - 19);
                        Array<Brick> bricks = bricksMap.get(removeX);
                        for (Brick brick : bricks) {
                            brickGroup.removeActor(brick);
                        }
                    }
                }
            });
        }

        brickGroup.addAction(Actions.sequence(actions));
    }


    private void showNextShapeAndGenerateAnotherNext() {
        shape = null;
        shape = nextShape;
        nextShape = null;

        shape.addToGroup(brickGroup);
    }

    private void absorbShape(AbstractShape shape) {
        bricksArray.addAll(shape.generateBricks());
        for (Brick brick : shape.generateBricks()) {
            int locX = brick.getLocX();
            Array<Brick> rowArray = bricksMap.get(locX);
            if (rowArray == null) {
                rowArray = new Array<Brick>();
            }
            rowArray.add(brick);
            bricksMap.put(locX, rowArray);
        }
    }


    private void checkShapeAndNextShapeExistState() {

        if (shape == null) {
            shape = ShapeFactory.buildShape(bricksArray, bricksMap);
            shape.addToGroup(brickGroup);
        }

        if (nextShape == null) {
            disableDownTouchState();
            nextShape = ShapeFactory.buildShape(bricksArray, bricksMap);
            showNextShape(nextShape);
        }
    }

    private void showNextShape(AbstractShape shape) {
        nextShapeGroup.clearChildren();

        int minusX = shape.getMinusX();
        int minusY = shape.getMinusY();

        for (Brick brick : shape.generateBricks()) {
            IndicatorBrick indicatorBrick = new IndicatorBrick(brick.getLocX() - minusX, brick.getLocY() - minusY);
            nextShapeGroup.addActor(indicatorBrick);
        }
    }

    private void disableDownTouchState() {
        isDownTouch = false;
        if (bottom.isPressed()) {
            isDownTouchTimeCaculate = false;
            downDownTime = System.currentTimeMillis() + 1000;
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        update(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
