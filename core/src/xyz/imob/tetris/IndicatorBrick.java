package xyz.imob.tetris;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import xyz.imob.tetris.utils.ActorHelper;
import xyz.imob.tetris.utils.ResourceLoader;
import xyz.imob.tetris.utils.SizeUtils;

/**
 * Created by glzlaohuai on 2017/2/25.
 */

public class IndicatorBrick extends Image {

    private static final Vector2 BLOCK_SIZE = SizeUtils.formatSize(39, 39, false);

    private static final Vector2 BLOCK_MARGIN = SizeUtils.formatSize(1, 1, false);

    private static final Vector2 BLOCK_START_POSITION = new Vector2(687 + BLOCK_MARGIN.x, 1170 + BLOCK_MARGIN.y);

    private int locX;
    private int locY;

    public IndicatorBrick(int x, int y) {
        super(ResourceLoader.blockFill);
        ActorHelper.setSize(this, 39, 39, false);
        this.locX = x;
        this.locY = y;

        setPosition(BLOCK_START_POSITION.x + y * (BLOCK_SIZE.x + BLOCK_MARGIN.x), BLOCK_START_POSITION.y + x * (BLOCK_SIZE.y + BLOCK_MARGIN.y));
    }

    public void setLocX(int locX) {
        this.locX = locX;
        setY(GameScreen.BLOCK_START_POSITION.y + locX * (BLOCK_SIZE.y + BLOCK_MARGIN.y));
    }

    public void setLocY(int locY) {
        this.locY = locY;
        setX(GameScreen.BLOCK_START_POSITION.x + locY * (BLOCK_SIZE.x + BLOCK_MARGIN.x));
    }

}
