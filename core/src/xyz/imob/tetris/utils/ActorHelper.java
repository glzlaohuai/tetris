package xyz.imob.tetris.utils;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Frustum;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;

/**
 * Created by glzlaohuai on 2016/12/16.
 */

public class ActorHelper {


    public static Vector2[] getOutRectFourVectors(Actor actor) {

        float degrees = actor.getRotation();
        float originX = actor.getOriginX();
        float originY = actor.getOriginY();
        float width = actor.getWidth();
        float height = actor.getHeight();
        float scaleX = actor.getScaleX();
        float scaleY = actor.getScaleY();
        float x = actor.getX();
        float y = actor.getY();

        float cos = MathUtils.cosDeg(degrees);
        float sin = MathUtils.sinDeg(degrees);
        float fx = -originX;
        float fy = -originY;
        float fx2 = width - originX;
        float fy2 = height - originY;

        if (scaleX != 1 || scaleY != 1) {
            fx *= scaleX;
            fy *= scaleY;
            fx2 *= scaleX;
            fy2 *= scaleY;
        }

        float worldOriginX = x + originX;
        float worldOriginY = y + originY;

        float x1 = cos * fx - sin * fy + worldOriginX;
        float y1 = sin * fx + cos * fy + worldOriginY;

        float x2 = cos * fx2 - sin * fy + worldOriginX;
        float y2 = sin * fx2 + cos * fy + worldOriginY;

        float x3 = cos * fx2 - sin * fy2 + worldOriginX;
        float y3 = sin * fx2 + cos * fy2 + worldOriginY;

        float x4 = x1 + (x3 - x2);
        float y4 = y3 - (y2 - y1);

        return new Vector2[]{new Vector2(x1, y1), new Vector2(x2, y2), new Vector2(x3, y3), new Vector2(x4, y4)};
    }


    public static int getPointDegree(Actor fromActor, Actor toActor) {

        float fromX = fromActor.getX(Align.center);
        float fromY = fromActor.getY(Align.center);

        float toX = toActor.getX(Align.center);
        float toY = toActor.getY(Align.center);

        int degree = (int) Math.toDegrees(Math.atan2(toY - fromY, toX - fromX));
        if (degree < 0) degree += 360;
        return degree;
    }

    public static boolean isRoundActorInCamera(Actor actor, Camera camera) {
        Frustum frustum = camera.frustum;
        boolean result = frustum.sphereInFrustum(actor.getX(Align.center), actor.getY(Align.center), 0, actor.getWidth() / 2);
        return result;
    }


    public static void setSize(Actor actor, float width, float height) {
        setSize(actor, width, height, true);
    }

    public static void setSize(Actor actor, float width, float height, boolean fitWidth) {
        Vector2 vector2 = SizeUtils.formatSize(width, height, fitWidth);
        actor.setSize(vector2.x, vector2.y);
    }

}
