package xyz.imob.tetris.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by glzlaohuai on 2016/12/23.
 */

public class I18NUtils {

    public static final String VALUE_FILE_NAME = "values/strings%s.json";

    //语言代码表http://www.lingoes.cn/zh/translator/langcode.htm
    private static final String[] SUPPORT_LAN = {"zh", "en"};
    private static List<String> SUPPORTLANLIST = Arrays.asList(SUPPORT_LAN);
    private static final String DEFAULT_LAN = "en";

    private static JsonValue JSON_VALUE;

    public static String getLanguage() {
        Locale locale = Locale.getDefault();
        String lan = locale.getLanguage().substring(0, 2);
        if (!SUPPORTLANLIST.contains(lan))
            lan = DEFAULT_LAN;
        return lan;
    }

    private static String getValue(String key, String file) {
        if (JSON_VALUE == null) {
            JsonReader jsonReader = new JsonReader();
            FileHandle fileHandle = Gdx.files.internal(file);
            JsonValue jsonValue = jsonReader.parse(fileHandle);
            JSON_VALUE = jsonValue;
        }
        return JSON_VALUE.getString(key);
    }


    /**
     * 根据设备语言设置，从保存着文本内容的json文件（也就是assets/values目录下的strings_[language].json）中获取key所对应的内容
     *
     * @param key
     * @return
     */
    public static String getValue(String key) {
        String language = getLanguage();
        String file = String.format(VALUE_FILE_NAME, "_" + language);
        return getValue(key, file);
    }


}
