package xyz.imob.tetris.utils;

import com.badlogic.gdx.audio.Music;

/**
 * Created by glzlaohuai on 2017/1/15.
 */

public class MusicUtils {

    public static void playMusic(Music music) {
        if (GameSaver.isSoundOpen()) {
            music.play();
        }
    }

    public static void stop(Music music) {
        if (music.isPlaying()) {
            music.stop();
        }
    }
}
