package xyz.imob.tetris.utils;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by glzlaohuai on 2017/1/7.
 */

public class SoundUtils {

    private static boolean SOUND_ON = GameSaver.isSoundOpen();

    //在每次更新设置之后，调用该方法
    public static void refreshSoundOnValue() {
        SOUND_ON = GameSaver.isSoundOpen();
    }

    public static void playSound(Sound sound) {
        playSound(sound, 1);
    }

    public static void playSound(Sound sound, float volume) {
        if (SOUND_ON) {
            long id = sound.play();
            sound.setVolume(id, volume);
        }
    }


    public static void playMusic(Music music) {
        if (SOUND_ON) {
            music.play();
        }
    }

    public static void stopMusic(Music music) {
        if (music.isPlaying()) {
            music.stop();
        }
    }

}
