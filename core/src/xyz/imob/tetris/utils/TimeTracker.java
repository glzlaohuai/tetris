package xyz.imob.tetris.utils;

/**
 * Created by glzlaohuai on 2016/12/27.
 */

public class TimeTracker {

    private long time = 0;
    private String method = "";

    public void start(String method) {
        time = System.currentTimeMillis();
        this.method = method;
    }

    public void end() {
//        System.out.println("TimeTracker: " + method + " " + (endTime - time));
    }
}
