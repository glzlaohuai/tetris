package xyz.imob.tetris.utils;

import com.badlogic.gdx.utils.Array;

import java.util.Map;
import java.util.Random;

import xyz.imob.tetris.Brick;
import xyz.imob.tetris.shapes.AbstractShape;
import xyz.imob.tetris.shapes.ShapeI;
import xyz.imob.tetris.shapes.ShapeL;
import xyz.imob.tetris.shapes.ShapeS;
import xyz.imob.tetris.shapes.ShapeSquare;
import xyz.imob.tetris.shapes.ShapeT;


/**
 * Created by glzlaohuai on 2017/2/19.
 */

public class ShapeFactory {

    private static final int MAX_SHAPE_NUM = 5;

    private static Random random = new Random();

    public static AbstractShape buildShape(Array<Brick> brickArray, Map<Integer, Array<Brick>> bricksMap) {
        int nextInt = random.nextInt(MAX_SHAPE_NUM);
        int rotateNum = random.nextInt(5);

        AbstractShape shape = null;

        switch (nextInt) {
            case 0:
                shape = new ShapeI(brickArray, bricksMap);
                break;
            case 1:
                shape = new ShapeL(brickArray, bricksMap);
                break;
            case 2:
                shape = new ShapeS(brickArray, bricksMap);
                break;
            case 3:
                shape = new ShapeSquare(brickArray, bricksMap);
                break;
            case 4:
                shape = new ShapeT(brickArray, bricksMap);
                break;
            default:
                shape = new ShapeL(brickArray, bricksMap);
                break;
        }

        for (int i = 0; i < rotateNum; i++) {
            shape.rotate();
        }
        return shape;
    }


}
