package xyz.imob.tetris.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Logger;

import java.util.Arrays;
import java.util.List;

/**
 * Created by glzlaohuai on 2016/12/21.
 */

public class GameSaver {

    private static final String TAG = "GameSaver";

    private static Logger logger = new Logger(TAG, Logger.DEBUG);

    private static final String KEY_HIGH_SCORE = "high_score";
    private static final String KEY_OWNED_SKINS = "owned_skins";
    private static final String PREFERENCE_NAME = "com.imob.greedy.DATA";
    private static final String KEY_USED_SKIN = "used_skin";
    private static final String KEY_NICKNAME = "nickname";
    private static final String KEY_COIN_NUM = "coin_num";
    private static final String KEY_SOUND = "sound";

    private static final String DEFAULT_OWNED_SKIN_IDS = "0,1,2,3,4";
    private static final int DEFAULT_USED_SKIN_ID = 1;
    private static final String DEFAULT_NICKNAME = "╮(￣▽￣)╭";
    private static final int DEFAULT_COIN_NUM = 25;

    public static long getHighestScore() {
        return Gdx.app.getPreferences(PREFERENCE_NAME).getLong(KEY_HIGH_SCORE, 0);
    }

    public static void saveHighestScore(long score) {
        Gdx.app.getPreferences(PREFERENCE_NAME).putLong(KEY_HIGH_SCORE, score).flush();
    }

    public static List<Integer> getOwnedSkinIDs() {
        String idString = Gdx.app.getPreferences(PREFERENCE_NAME).getString(KEY_OWNED_SKINS, DEFAULT_OWNED_SKIN_IDS);
        String[] strIDs = idString.split(",");
        List<String> idsList = Arrays.asList(strIDs);
        Integer[] ids = new Integer[idsList.size()];
        for (int i = 0; i < ids.length; i++) {
            ids[i] = Integer.parseInt(idsList.get(i));
        }
        return Arrays.asList(ids);
    }

    public static void ownASkin(int id) {
        String nowOwnedSkinIds = Gdx.app.getPreferences(PREFERENCE_NAME).getString(KEY_OWNED_SKINS, DEFAULT_OWNED_SKIN_IDS) + "," + id;
        Gdx.app.getPreferences(PREFERENCE_NAME).putString(KEY_OWNED_SKINS, nowOwnedSkinIds).flush();
    }

    public static int getUsedSkinID() {
        int userSkinID = Gdx.app.getPreferences(PREFERENCE_NAME).getInteger(KEY_USED_SKIN, DEFAULT_USED_SKIN_ID);
        logger.info("当前使用的皮肤：" + userSkinID);
        return userSkinID;
    }

    public static void saveUsedSkinID(int skinID) {
        logger.info("选择了皮肤：" + skinID);
        Gdx.app.getPreferences(PREFERENCE_NAME).putInteger(KEY_USED_SKIN, skinID).flush();
    }

    public static String getNickName() {
        return Gdx.app.getPreferences(PREFERENCE_NAME).getString(KEY_NICKNAME, DEFAULT_NICKNAME);
    }

    public static void saveNickName(String nickName) {
        Gdx.app.getPreferences(PREFERENCE_NAME).putString(KEY_NICKNAME, nickName).flush();
    }

    public static int getCoinNum() {
        return Gdx.app.getPreferences(PREFERENCE_NAME).getInteger(KEY_COIN_NUM, DEFAULT_COIN_NUM);
    }

    public static void saveCoinNum(int coinNum) {
        Gdx.app.getPreferences(PREFERENCE_NAME).putInteger(KEY_COIN_NUM, coinNum).flush();
    }

    public static boolean isSoundOpen() {
        return Gdx.app.getPreferences(PREFERENCE_NAME).getBoolean(KEY_SOUND, true);
    }

    public static void setSound(boolean on) {
        Gdx.app.getPreferences(PREFERENCE_NAME).putBoolean(KEY_SOUND, on).flush();
    }


}
