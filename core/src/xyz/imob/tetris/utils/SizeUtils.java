package xyz.imob.tetris.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import xyz.imob.tetris.others.Constants;

/**
 * Created by glzlaohuai on 2017/1/26.
 */

public class SizeUtils {

    public static Vector2 formatSize(float width, float height) {

        //宽高比
        float ratio = width / height;

        float widthRatio = width / Constants.WIDTH;
        float realWidth = widthRatio * Gdx.graphics.getWidth();
        float realHeight = realWidth / ratio;

        float toWidth = realWidth / Gdx.graphics.getWidth() * Constants.WIDTH;
        float toHeight = realHeight / Gdx.graphics.getHeight() * Constants.HEIGHT;

        return new Vector2(toWidth, toHeight);
    }

    public static Vector2 formatSize(float width, float height, boolean fitWidth) {
        //宽高比
        float ratio = width / height;

        float toWidth;
        float toHeight;

        if (fitWidth) {
            float widthRatio = width / Constants.WIDTH;
            float realWidth = widthRatio * Gdx.graphics.getWidth();
            float realHeight = realWidth / ratio;

            toWidth = realWidth / Gdx.graphics.getWidth() * Constants.WIDTH;
            toHeight = realHeight / Gdx.graphics.getHeight() * Constants.HEIGHT;
        } else {
            float heightRatio = height / Constants.HEIGHT;
            float realHeight = heightRatio * Gdx.graphics.getHeight();
            float realWidth = realHeight * ratio;

            toWidth = realWidth / Gdx.graphics.getWidth() * Constants.WIDTH;
            toHeight = realHeight / Gdx.graphics.getHeight() * Constants.HEIGHT;
        }

        return new Vector2(toWidth, toHeight);
    }

}
