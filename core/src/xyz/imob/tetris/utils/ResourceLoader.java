package xyz.imob.tetris.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;

import net.mwplay.nativefont.NativeFont;
import net.mwplay.nativefont.NativeFontPaint;

/**
 * Created by glzlaohuai on 2016/11/28 上午10:44.
 */

public class ResourceLoader {

    private static AssetManager assetManager;

    static {
        initAssetmanager();
        NativeFont.setRobovm();
    }

    public static Texture bg;
    public static Texture blockFill;
    public static Texture blockNone;
    public static Texture btnGreenNormal;
    public static Texture btnGreenPress;
    public static Texture btnRedNormal;
    public static Texture btnRedPress;
    public static Texture btnYellowNormal;
    public static Texture btnYellowPress;
    public static Texture frame;
    public static Texture coffee;
    public static Texture sound;
    public static Texture outLine;
    public static Texture blockBg;
    public static Texture touchEffect;
    public static Texture nextShapeBg;

    public static Music bgMusic;
    public static Sound rotateSound;
    public static Sound gameOverSound;
    public static Sound moveSound;
    public static Sound directSound;
    public static Sound hitSound;
    public static Sound clearSound;

    public static BitmapFont font40;
    public static NativeFont font25;

    private static void initAssetmanager() {
        assetManager = new AssetManager();
        FileHandleResolver resolver = new InternalFileHandleResolver();
        assetManager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        assetManager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));
    }

    public static void loadResource() {
        if (assetManager == null) {
            initAssetmanager();
        }

        TextureLoader.TextureParameter textureParameter = new TextureLoader.TextureParameter();
        textureParameter.magFilter = Texture.TextureFilter.Linear;
        textureParameter.minFilter = Texture.TextureFilter.Linear;
        textureParameter.format = Pixmap.Format.RGBA8888;

        assetManager.load("bg.png", Texture.class, textureParameter);
        assetManager.load("block_fill.png", Texture.class, textureParameter);
        assetManager.load("block_none.png", Texture.class, textureParameter);
        assetManager.load("btn_green_normal.png", Texture.class, textureParameter);
        assetManager.load("btn_green_press.png", Texture.class, textureParameter);
        assetManager.load("btn_red_normal.png", Texture.class, textureParameter);
        assetManager.load("btn_red_press.png", Texture.class, textureParameter);
        assetManager.load("btn_yellow_normal.png", Texture.class, textureParameter);
        assetManager.load("btn_yellow_press.png", Texture.class, textureParameter);
        assetManager.load("frame.png", Texture.class, textureParameter);
        assetManager.load("icon_coffee.png", Texture.class, textureParameter);
        assetManager.load("icon_sound.png", Texture.class, textureParameter);
        assetManager.load("out_line.png", Texture.class, textureParameter);
        assetManager.load("block_bg.png", Texture.class, textureParameter);
        assetManager.load("touch_effect.png", Texture.class, textureParameter);
        assetManager.load("next_shape_bg.png", Texture.class, textureParameter);

        FreetypeFontLoader.FreeTypeFontLoaderParameter size1Params = new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        size1Params.fontFileName = "font.ttf";
        size1Params.fontParameters.size = 40;
        size1Params.fontParameters.magFilter = Texture.TextureFilter.Linear;
        size1Params.fontParameters.minFilter = Texture.TextureFilter.Linear;

        assetManager.load("30_font.ttf", BitmapFont.class, size1Params);

        assetManager.load("sounds/bg_music.mp3", Music.class);
        assetManager.load("sounds/lost.mp3", Sound.class);
        assetManager.load("sounds/move.mp3", Sound.class);
        assetManager.load("sounds/rotate.mp3", Sound.class);
        assetManager.load("sounds/fire.mp3", Sound.class);
        assetManager.load("sounds/hit.mp3", Sound.class);
        assetManager.load("sounds/clear.mp3", Sound.class);

        assetManager.finishLoading();

        bg = assetManager.get("bg.png");
        blockFill = assetManager.get("block_fill.png");
        blockNone = assetManager.get("block_none.png");
        btnGreenNormal = assetManager.get("btn_green_normal.png");
        btnGreenPress = assetManager.get("btn_green_press.png");
        btnRedNormal = assetManager.get("btn_red_normal.png");
        btnRedPress = assetManager.get("btn_red_press.png");
        btnYellowNormal = assetManager.get("btn_yellow_normal.png");
        btnYellowPress = assetManager.get("btn_yellow_press.png");
        frame = assetManager.get("frame.png");
        coffee = assetManager.get("icon_coffee.png");
        sound = assetManager.get("icon_sound.png");
        outLine = assetManager.get("out_line.png");
        blockBg = assetManager.get("block_bg.png");
        touchEffect = assetManager.get("touch_effect.png");
        nextShapeBg = assetManager.get("next_shape_bg.png");

        font40 = assetManager.get("30_font.ttf");
        font25 = new NativeFont(new NativeFontPaint(25));

        bgMusic = assetManager.get("sounds/bg_music.mp3");
        gameOverSound = assetManager.get("sounds/lost.mp3");
        rotateSound = assetManager.get("sounds/rotate.mp3");
        moveSound = assetManager.get("sounds/move.mp3");
        directSound = assetManager.get("sounds/fire.mp3");
        hitSound = assetManager.get("sounds/hit.mp3");
        clearSound = assetManager.get("sounds/clear.mp3");
    }

    public static void freeResource() {
        assetManager.clear();
        assetManager.dispose();
        assetManager = null;

        try {
            font25.dispose();
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

}
