package xyz.imob.tetris;

/**
 * Created by glzlaohuai on 2017/2/19.
 */

public class TimeTicker {

    private float pass = 0;
    private int tickMs;

    public TimeTicker(int tickMs) {
        this.tickMs = tickMs;
    }


    public void setTickMs(int tickMs) {
        this.tickMs = tickMs;
    }

    public void tick(float deltaSec) {
        pass += deltaSec * 1000;
    }

    public boolean trigger() {
        if (pass / tickMs >= 1) {
            pass = 0;
            return true;
        }
        return false;
    }

}
