package xyz.imob.tetris.shapes;

/**
 * Created by glzlaohuai on 2017/2/19.
 */

public interface Shape {

    boolean canRotate();

    void rotate();

    AbstractShape.HitState checkHitState();

}
