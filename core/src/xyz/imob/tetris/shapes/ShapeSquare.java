package xyz.imob.tetris.shapes;

import com.badlogic.gdx.utils.Array;

import java.util.Map;

import xyz.imob.tetris.Brick;

/**
 * Created by glzlaohuai on 2017/2/21.
 */

public class ShapeSquare extends AbstractShape {

    private Array<Brick> brickArray;

    public ShapeSquare(Array<Brick> bricksArray, Map<Integer, Array<Brick>> bricksMap) {
        super(bricksArray, bricksMap, false, false);
    }

    @Override
    public Array<Brick> generateBricks() {
        if (brickArray == null || brickArray.size == 0) {
            brickArray = new Array<Brick>();
            brickArray.add(new Brick(18, 5));
            brickArray.add(new Brick(19, 5));
            brickArray.add(new Brick(18, 6));
            brickArray.add(new Brick(19, 6));
        }
        return brickArray;
    }

    @Override
    protected Brick getOriginBrick() {
        return generateBricks().get(0);
    }
}
