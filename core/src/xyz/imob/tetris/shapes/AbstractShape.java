package xyz.imob.tetris.shapes;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

import java.util.Map;

import xyz.imob.tetris.Brick;

/**
 * Created by glzlaohuai on 2017/2/19.
 */

public abstract class AbstractShape implements xyz.imob.tetris.shapes.Shape {

    private Array<Brick> bricksArray;
    private Map<Integer, Array<Brick>> bricksMap;

    private int rotateNum = 0;

    //在下一帧时候会'确认'hit的状态，在下一帧到来之前，还可以进行移动或者rotate操作
    private boolean willHitInNextFrame = false;

    private boolean isHit = false;

    private boolean rotatable;
    private boolean allRotate;


    public AbstractShape(Array<Brick> bricksArray, Map<Integer, Array<Brick>> bricksMap, boolean rotatable, boolean allRotate) {
        this.bricksArray = bricksArray;
        this.bricksMap = bricksMap;
        this.rotatable = rotatable;
        this.allRotate = allRotate;

    }

    //将该形状添加到stage中
    public void addToGroup(Group group) {
        for (Brick brick : generateBricks()) {
            group.addActor(brick);
        }
    }


    public boolean contains(int x, int y) {
        boolean result = false;
        for (Brick brick : generateBricks()) {
            if (brick.getLocX() == x && brick.getLocY() == y) {
                result = true;
                break;
            }
        }
        return result;
    }

    //获取构成该形状的brick
    public abstract Array<Brick> generateBricks();

    //获取中心brick
    protected abstract Brick getOriginBrick();


    @Override
    public boolean canRotate() {

        if (!rotatable) return false;

        boolean clockWise;

        int nextRotateNum = rotateNum + 1;

        if (allRotate || nextRotateNum == 1) {
            clockWise = true;
        } else {
            clockWise = false;
        }

        Brick originBrick = getOriginBrick();

        int originLocX = originBrick.getLocX();
        int originLocY = originBrick.getLocY();


        if (clockWise) {
            int minusY = getMinusY();

            if (minusY != originLocY) {
                int toX = originLocX + originLocY - minusY;
                int toY = minusY;

                for (int x = originLocX + 1; x <= toX; x++) {
                    for (int y = originLocY - 1; y >= toY; y--) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }

            int maxY = getMaxY();

            if (maxY != originLocY) {
                int toX = originLocX - (maxY - originLocY);
                int toY = maxY;

                if (toX < 0) {
                    return false;
                }

                for (int x = originLocX - 1; x >= toX; x--) {
                    for (int y = originLocY + 1; y <= toY; y++) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }


            int minusX = getMinusX();

            if (minusX != originLocX) {
                int toY = originLocY - (originLocX - minusX);
                int toX = minusX;

                if (toY < 0) {
                    return false;
                }

                for (int x = originLocX - 1; x >= toX; x--) {
                    for (int y = originLocY - 1; y >= toY; y--) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }

            int maxX = getMaxX();

            if (maxX != originLocX) {

                int toX = maxX;
                int toY = originLocY + (maxX - originLocX);

                if (toY > 9) {
                    return false;
                }

                for (int x = originLocX + 1; x <= toX; x++) {
                    for (int y = originLocY + 1; y <= toY; y++) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }

            return true;

        } else {

            int minusY = getMinusY();

            if (minusY != originLocY) {
                int toX = originLocX - (originLocY - minusY);
                int toY = minusY;

                if (toX < 0) {
                    return false;
                }

                for (int x = originLocX - 1; x >= toX; x--) {
                    for (int y = originLocY - 1; y >= toY; y--) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }

            int maxY = getMaxY();

            if (maxY != originLocY) {
                int toX = originLocX + (maxY - originLocY);
                int toY = maxY;

                for (int x = originLocX + 1; x <= toX; x++) {
                    for (int y = originLocY + 1; y <= toY; y++) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }


            int minusX = getMinusX();

            if (minusX != originLocX) {
                int toY = originLocY + (originLocX - minusX);
                int toX = minusX;

                if (toY > 9) {
                    return false;
                }

                for (int x = originLocX - 1; x >= toX; x--) {
                    for (int y = originLocY + 1; y <= toY; y++) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }

            int maxX = getMaxX();

            if (maxX != originLocX) {

                int toX = maxX;
                int toY = originLocY - (maxX - originLocX);

                if (toY < 0) {
                    return false;
                }

                for (int x = originLocX + 1; x <= toX; x++) {
                    for (int y = originLocY - 1; y >= toY; y--) {
                        if (y < 0 || y > 9 || isLocFilled(x, y)) return false;
                    }
                }
            }

            return true;


        }
    }


    public int getMinusY() {
        int minusY = getOriginBrick().getLocY();
        for (Brick brick : generateBricks()) {
            if (minusY > brick.getLocY()) {
                minusY = brick.getLocY();
            }
        }
        return minusY;
    }

    public int getMinusX() {
        int minusX = getOriginBrick().getLocX();
        for (Brick brick : generateBricks()) {
            if (minusX > brick.getLocX()) {
                minusX = brick.getLocX();
            }
        }
        return minusX;
    }

    private int getMaxY() {
        int maxY = getOriginBrick().getLocY();
        for (Brick brick : generateBricks()) {
            if (maxY < brick.getLocY()) {
                maxY = brick.getLocY();
            }
        }
        return maxY;
    }

    private int getMaxX() {
        int maxX = getOriginBrick().getLocX();
        for (Brick brick : generateBricks()) {
            if (maxX < brick.getLocX()) {
                maxX = brick.getLocX();
            }
        }
        return maxX;
    }

    //检查该位置是否被填充
    private boolean isLocFilled(int locX, int locY) {
        Array<Brick> temp = bricksMap.get(locX);
        if (temp == null || temp.size == 0) {
            return false;
        }
        for (Brick brick : temp) {
            if (brick.getLocY() == locY) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void rotate() {
        if (isHit || !canRotate()) return;

        rotateNum++;
        rotateNum = rotateNum % 2;

        boolean clockWise;
        if (allRotate || rotateNum == 1) {
            clockWise = true;
        } else {
            clockWise = false;
        }

        Brick originBrick = getOriginBrick();

        int originLocX = originBrick.getLocX();
        int originLocY = originBrick.getLocY();

        for (Brick brick : generateBricks()) {
            int locX = brick.getLocX();
            int locY = brick.getLocY();

            int toLocY = originLocY + (locX - originLocX) * (clockWise ? 1 : -1);
            int toLocX = originLocX - (locY - originLocY) * (clockWise ? 1 : -1);

            brick.setLocX(toLocX);
            brick.setLocY(toLocY);
        }
    }


    public HitState checkHitState() {

        for (Brick brick : generateBricks()) {
            int locY = brick.getLocY();
            int locX = brick.getLocX();

            if (locX == 0) {
                if (willHitInNextFrame) {
                    isHit = true;
                    return HitState.HIT;
                } else {
                    willHitInNextFrame = true;
                    return HitState.WILL_HIT;
                }
            } else {
                Array<Brick> belowBricksArray = bricksMap.get(locX - 1);
                if (belowBricksArray == null || belowBricksArray.size == 0) {
                } else {
                    for (Brick belowBrick : belowBricksArray) {
                        if (belowBrick.getLocY() == locY) {
                            if (willHitInNextFrame) {
                                isHit = true;
                                return HitState.HIT;
                            } else {
                                willHitInNextFrame = true;
                                return HitState.WILL_HIT;
                            }
                        }
                    }
                }
            }
        }
        willHitInNextFrame = false;
        return HitState.HIT_NONE;
    }

    public void moveDown() {
        for (Brick brick : generateBricks()) {
            brick.moveDown();
        }
    }

    public boolean canMoveLeft() {
        if (isHit) return false;
        for (Brick brick : generateBricks()) {

            int locX = brick.getLocX();
            int locY = brick.getLocY();

            if (locY == 0) {
                return false;
            }

            Array<Brick> rowBricks = bricksMap.get(locX);

            if (rowBricks == null || rowBricks.size == 0) {
                continue;
            } else {
                for (Brick existBrick : rowBricks) {
                    if (existBrick.getLocY() == locY - 1) return false;
                }
            }
        }
        return true;
    }

    public boolean canMoveRight() {
        if (isHit) return false;
        for (Brick brick : generateBricks()) {

            int locX = brick.getLocX();
            int locY = brick.getLocY();

            if (locY == 9) {
                return false;
            }

            Array<Brick> rowBricks = bricksMap.get(locX);

            if (rowBricks == null || rowBricks.size == 0) {
                continue;
            } else {
                for (Brick existBrick : rowBricks) {
                    if (existBrick.getLocY() == locY + 1) return false;
                }
            }
        }
        return true;
    }


    public void moveLeft() {
        if (!isHit && canMoveLeft()) {
            for (Brick brick : generateBricks()) {
                brick.setLocY(brick.getLocY() - 1);
            }
        }
    }


    public void moveRight() {
        if (!isHit && canMoveRight()) {
            for (Brick brick : generateBricks()) {
                brick.setLocY(brick.getLocY() + 1);
            }
        }
    }


    //代表了旋转能力,360旋转，90度旋转
    public enum HitState {
        WILL_HIT, HIT, HIT_NONE
    }

}
