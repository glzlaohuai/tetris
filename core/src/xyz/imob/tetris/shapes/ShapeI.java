package xyz.imob.tetris.shapes;

import com.badlogic.gdx.utils.Array;

import java.util.Map;

import xyz.imob.tetris.Brick;

/**
 * Created by glzlaohuai on 2017/2/19.
 */

public class ShapeI extends AbstractShape {

    private Array<Brick> shapeBricksArray;

    public ShapeI(Array<Brick> bricksArray, Map<Integer, Array<Brick>> bricksMap) {
        super(bricksArray, bricksMap, true, false);
    }

    @Override
    public Array<Brick> generateBricks() {
        if (shapeBricksArray == null) {
            shapeBricksArray = new Array<Brick>();
            shapeBricksArray.add(new Brick(18, 5));
            shapeBricksArray.add(new Brick(19, 5));
            shapeBricksArray.add(new Brick(20, 5));
            shapeBricksArray.add(new Brick(21, 5));
        }
        return shapeBricksArray;
    }

    @Override
    protected Brick getOriginBrick() {
        return generateBricks().get(1);
    }

    public void moveDown() {
        for (Brick brick : generateBricks()) {
            brick.moveDown();
        }
    }
}
