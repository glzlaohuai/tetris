package xyz.imob.tetris.others;

/**
 * Created by glzlaohuai on 2017/2/18.
 */

public class Constants {
    public static final int WIDTH = 1080;
    public static final int HEIGHT = 1920;

    //方格距离底部边框的边距
    public static final int BLOCK_BOTTOM_PADDING = 15;

    public static final int BLOCK_ROW = 19;
    public static final int BLOCK_COL = 10;

    //游戏区域距离黑线的距离
    public static final int GAME_AREA_MARGIN = 36;
    //游戏区域距离外边黑线框的距离
    public static final int GAME_AREA_PADDING = 26;

    public static final String ANDROID_MORE = "https://play.google.com/store/apps/developer?id=%E5%88%98%E7%AB%8B%E5%B3%B0";
    public static final String IOS_MORE = "https://itunes.apple.com/cn/developer/lifeng-liu/id1198054659";

}
