package xyz.imob.tetris;

import android.app.Application;

import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryAgentListener;

/**
 * Created by glzlaohuai on 2017/2/25.
 */

public class XApplication extends Application {

    private static final String FLURRY_API_KEY = "SDMN4QGFDW8HN5P4YBKP";

    @Override
    public void onCreate() {
        super.onCreate();

        new FlurryAgent.Builder()
                .withLogEnabled(true)
                .withListener(new FlurryAgentListener() {
                    @Override
                    public void onSessionStarted() {

                    }
                })
                .build(this, FLURRY_API_KEY);
    }
}
